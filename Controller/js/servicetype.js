var path = "../Controller/php/OpsServiceType.php";
var idServiceType = -1;
var editable = false;
var unitTime = -1;
var unitOdometer = -1;
//------------------------------------------------------------------------------
function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()
{
    resetTable();
    resetDdl();
}
function resetTable()
{
    ajax_call(path,{type:'list_service_type'},"service_type_div");
    $('#dataTables-example').DataTable({
        responsive: true
    });
}
function clean()
{
    $('#txt_NameServiceType').val('');
    $('#txt_Time').val('');
    $('#txt_Odometer').val('');
    unitTime = -1;
    unitOdometer = -1;
    resetDdl();
}
function resetDdl(){
    ajax_call(path,{type:'list_time',title:'Tiempo'},"catalogUnitTime");
    ajax_call(path,{type:'list_odometer',title:'Distancia'},"catalogOdometer");
    $('.selectpicker').selectpicker('show');
}
function disableEnter(e)
{
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
}
function editColumn(e, dataNo, newValue, colName, txtFieldName)
{
    if(e.keyCode == 13)
    {          
       edit_data(dataNo,newValue, colName, txtFieldName);
    }
}
function edit_data(id, texto, column_name, txtFieldName)  
{   
    $.post( path, { 
            type:'update_service_type', 
            id: id, 
            column_name:column_name, 
            texto:texto
        }).done(function(data) 
        {
            if(data > 0){
                highlightRow("#8dc70a", txtFieldName);
                resetTable();
            }else{
                highlightRow("#d64b2f", txtFieldName);}
        });
}
function highlightRow(bgColor, txtFieldName)
{
    var rowSelector = $("#" + txtFieldName);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function message(message, iconType)
{
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}
//------------------------------------------------------------------------------
$(document).ready(function() {
    init();
});
//show
$(document).on('click', '.ServiceTypeClass', function(){
    idServiceType = $(this).data("id0");
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'show_service_type',
            idServiceType:idServiceType
        },  
        dataType:"text",  
        success:function(d){
            if(d == '')
                message("Hubo un problema al recuperar la información de este servicio.", "error");
            else{
                var obj = JSON.parse(d);
                $('#txt_NameServiceType').val(obj[0][1]);
                $('#txt_Time').val(obj[0][2]);
                ajax_call(path,{type:'list_time',title:obj[0][3]},"catalogUnitTime");
                $('#txt_Odometer').val(obj[0][4]);
                ajax_call(path,{type:'list_odometer',title:obj[0][5]},"catalogOdometer");
                $('.selectpicker').selectpicker('show');
                editable = true;
            }
        }  
    });
});
//delete
$(document).on('click', '#btn_delete', function(){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'remove_service_type',
            idServiceType:idServiceType
        },  
        dataType:"text",  
        success:function(d){
            if(d > 0){
                message("El tipo de servicio se ha eliminado correctamente.", "success");
                resetTable();
                resetDdl();
                clean();
                idServiceType = -1;
            }else
                message("Hubo un problema al guardar el tipo de servicio.", "error");
        }  
    });
});
//update
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDdl = this.id;
    var selected = $(this).find("option:selected").val();
    var column_name = '';
    if(nameDdl === 'ddl_time_service')
    {
        column_name = 'unitTime';
        unitTime = selected;
    }
    else if(nameDdl === 'ddl_unit_odometer')
    {
        column_name = 'unitOdometer';
        unitOdometer = selected;
    }
    //
    if(editable){
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{ 
                type:'update_service_type',
                id:idServiceType,
                column_name:column_name,
                texto:selected
            },dataType:"text",
            success:function(Id){
                if(Id > 0){
                    message("El tipo de servicio " + column_name + " se ha actualizado correctamente.", "success");
                    resetTable();
                }else{
                    message("Hubo un problema al actualizar el tipo de servicio " + column_name + ".", "error");
                }
            }
        });
    }
});
$(document).on('keyup', '.EditServiceType', function(e){  
    editColumn(e, idServiceType, $("#txt_NameServiceType").val(), "nameServiceType","txt_NameServiceType");
});
$(document).on('keyup', '.EditTime', function(e){  
    editColumn(e, idServiceType, $("#txt_Time").val(), "timeServiceType","txt_Time");
});
$(document).on('keyup', '.EditOdometer', function(e){  
    editColumn(e, idServiceType, $("#txt_Odometer").val(), "Odometer","txt_Odometer");
});
$(document).on('keydown', '.EditServiceType', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.EditTime', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.EditOdometer', function(e){
    disableEnter(e);
});
//cancel
$(document).on('click', '#btn_cancel', function(){
    clean();
    editable = false;
    idServiceType = -1;
});
//save
$(document).on('click', '#btn_save', function(){
    var name = $('#txt_NameServiceType').val();
    var time = $('#txt_Time').val();
    var odometer = $('#txt_Odometer').val();

    if(name === '') {message("Debe ingresar un nombre para el tipo de servicio.", "info"); return;}
    if(time === '') {message("Debe ingresar un tiempo para el tipo de servicio.", "info"); return;}
    if(unitTime < 0) { message("Debe seleccionar una unidad de tiempo para el tipo de servicio.", "info"); return;}
    if(odometer === '') {message("Debe ingresar la lectura para el tipo de servicio.", "info"); return;}
    if(unitOdometer < 0) { message("Debe seleccionar una unidad de distancia para el tipo de servicio.", "info"); return;}

    if(idServiceType < 0)
    {
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{ 
                type:'add_service_type',  
                name:name,
                time:time,
                unitTime:unitTime,
                odometer:odometer,
                unitOdometer:unitOdometer
            },  
            dataType:"text",  
            success:function(newId){
                if(newId > 0){
                    message("El tipo de servicio se ha guardado correctamente.", "success");
                    clean();
                    resetTable();
                    resetDdl();
                }else{
                    message("Hubo un problema al guardar el tipo de servicio.", "error");
                }
            }
        });
    }
});
