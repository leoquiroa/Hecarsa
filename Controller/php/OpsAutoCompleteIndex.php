<?php
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();      
    
    $output = '';
    $type_data = isset($_GET['type']) ? $_GET['type'] : '';
    
    switch ($type_data)
    {
        case 'ByName':
            $result = $sqlOps->sql_multiple_rows("CALL person_autocomplete('".$_GET['term']."')");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $data = array();
                $ix=0;
                while($row = $result->fetch_assoc()){
                    $data[$ix] = array("id"=>$row["idPerson"], "label"=>$row["namePerson"]);
                    $ix++;
                }
                echo json_encode($data);
            }
        break;
        case 'ByPlate':
            $result = $sqlOps->sql_multiple_rows("CALL plate_autocomplete('".$_GET['term']."')");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $data = array();
                $ix=0;
                while($row = $result->fetch_assoc()){
                    $data[$ix] = array("id"=>$row["idperson"], "label"=>$row["plateNumber"]);
                    $ix++;
                }
                echo json_encode($data);
            }
        break;
    }
    echo $output == '' ? '' : $output;
    