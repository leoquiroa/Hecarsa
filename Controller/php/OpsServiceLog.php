<?php
include "../../Model/SqlOperations.php";
$sqlOps = new SqlOperations();    
include "../../Controller/php/general.php";
$fns = new generalFunctions();    

$output = '';
$type_data = isset($_POST['type']) ? $_POST['type'] : '';

switch ($type_data)
{
    case 'PersonHead':
        $result = $sqlOps->sql_single_row("CALL person_get(".$_POST['idPerson'].")");
        if($result != ''){
            $output = 
                "<p>"
                    . "Propietario: <span>".$result["legalName"]." / ".$result["namePerson"]."</span>"
                    . " - NIT: <span>".$result["NIT"]."</span>"
                    . " - Telefono: <span>".$result["Phone"]."</span>"
                    . " - Fecha de Nac.: <span>".$result["Birthday"]."</span>"
                . "</p>";
        }
    break;
    case 'VehiclesTable':
        $result = $sqlOps->sql_multiple_rows("CALL vehicleperclient_getByPerson(".$_POST['idPerson'].")");
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $list = '';
            while($row = $result->fetch_assoc()){
                $list .= '
                    <tr>
                        <td style="cursor:pointer;" class="vehicleList" data-id0="'.$row["idvehicleperclient"].'">'.$row["plateNumber"].'</td>
                        <td >'.$row["brand"].'</td>
                        <td >'.$row["color"].'</td>
                    </tr>';
            }
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Placa</th>
                        <th style="text-align:center;">Marca</th>
                        <th style="text-align:center;">Color</th>        
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        }
    break;
    
    case 'VehiclesHead':
        $result = $sqlOps->sql_single_row("CALL vehicleperclient_get(".$_POST['idVehicle'].")");
        if($result != ''){
            $output = 
                "<p>"
                    . "Tipo: <span>".$result["nameVehicleType"]."</span>"
                    . " - Placa: <span>".$result["PlateNumber"]."</span>"
                    . " - Marca: <span>".$result["Brand"]."</span>"
                    . " - Linea: <span>".$result["Line"]."</span>"
                    . " - Color: <span>".$result["Color"]."</span>"
                    . " - Modelo: <span>".$result["YearVehicle"]."</span>"
                . "</p>";
        }
    break;
    case 'ServicesTable':
        $result = $sqlOps->sql_multiple_rows("CALL service_ByVehicle(".$_POST['idVehicle'].")");
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $list = '';
            while($row = $result->fetch_assoc()){
                $list .= '
                    <tr>
                        <td style="cursor:pointer;" class="serviceList" data-id0="'.$row["idService"].'">'.$row["currentTimeService"].'</td>
                        <td >'.$row["nameServiceType"].'</td>
                        <td >'.$row["currentOdometer"].' '.$row["nameOdometerType"].'</td>
                        <td > Q. '.$row["cost"].'</td>
                        <td >'.$row["note"].'</td>
                    </tr>';
            }
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Fecha</th>
                        <th style="text-align:center;">Tipo</th>
                        <th style="text-align:center;">Odometro</th>        
                        <th style="text-align:center;">Costo</th>
                        <th style="text-align:center;">Notas</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        }else{
            $output .= '<<<<<<<<<<<<<<<<<< Sin registro de servicios >>>>>>>>>>>>>>>>>>>>>>>>>>';
        }
    break;
    
    case 'ServiceHead':
        $result = $sqlOps->sql_single_row("CALL service_get(".$_POST['idService'].")");
        if($result != ''){
            $output = 
                "<p>"
                    . "Fecha último servicio: <span>".$result["currentTimeService"]."</span>"
                    . " - Fecha siguiente servicio: <span>".$result["nextTimeService"]."</span>"
                    . " - Tipo de servicio: <span>".$result["nameServiceType"]."</span>"
                    . " - Último odometro: <span>".$result["currentOdometer"]." ".$result["nameOdometerType"]."</span>"
                    . " - Siguiente odometro: <span>".$result["nextOdometer"]." ".$result["nameOdometerType"]."</span>"
                    . " - Costo: <span>".$result["cost"]."</span>"
                    . " - Notas: <span>".$result["note"]."</span>"
                . "</p>";
        }
    break;
    case 'ServiceAlert':
        $result = $sqlOps->sql_single_row("CALL service_getAlerts(".$_POST['idService'].")");
        if($result != ''){
            $output = $result["alert"];
        }
    break;
    case 'ServiceDetails':
        $result = $sqlOps->sql_multiple_rows("CALL servicedetail_get(".$_POST['idService'].")");
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $list = '';
            while($row = $result->fetch_assoc()){
                $list .= '
                    <tr>
                        <td >'.$row["codePart"].'</td>
                        <td >'.$row["namePart"].'</td>
                        <td ><img src="../Multimedia/Parts/'.$row["imageUrl"].'" height="42" width="42"></td>
                        <td > Q. '.$row["PricePart"].'</td>
                        <td >'.$row["brandPart"].'</td>
                        <td >'.$row["note"].'</td>
                    </tr>';
            }
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Codigo</th>
                        <th style="text-align:center;">Parte</th>
                        <th style="text-align:center;">Imagen</th>
                        <th style="text-align:center;">Precio</th>        
                        <th style="text-align:center;">Marca</th>
                        <th style="text-align:center;">Notas</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        }else{
            $output .= '<<<<<<<<<<<<<<<<<< Sin registro de partes >>>>>>>>>>>>>>>>>>>>>>>>>>';
        }
    break;
    
    case 'print':
        session_start();
        $_SESSION['variable1'] = 'hola pollis';
        include "../../Controller/php/reportGeneration.php";
    break;
}
echo $output == '' ? '' : $output;