var path = "../Controller/php/OpsPerson.php";
var idPersonEdit = -1;
var editable = false;

function ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init(){ 
    resetTable();
    clean();
}
function resetTable(){
    ajax_call(path,{type:'list_person'},"person_list_div");
    $('#dataTables-example').DataTable({
        responsive: true
    });
}
function clean(){
    $('#txt_legalName').val('');
    $('#txt_namePerson').val('');
    $('#txt_NIT').val('');
    $('#txt_Phone').val('');
    $('#txt_Birthday').val('');
    idPersonEdit = -1;
}
// DELETE
function deletePerson(path,idPerson){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{ 
            type:'remove_person_db',
            idPerson:idPerson},  
        dataType:"text",
        success:function(Id){
            if(Id > 0){
                resetTable();
                message("La persona se ha eliminado correctamente.", "warn");
            }else{
                message("Hubo un problema al eliminar a la persona.", "error");
            }
        }
    });
}
// DISABLE ENTER
function disableEnter(e)
{
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
}
// EDIT NAME
function editColumn(e, dataNo, newValue, colName)
{
    if(e.keyCode == 13 && editable === true)
    {          
       edit_data(dataNo,newValue, colName);
    }
}
function edit_data(id, texto, column_name)  
{   
    $.post( path, { 
            type:'update_person', 
            id: id, 
            column_name:column_name, 
            texto:texto
        }).done(function(data) 
        {
            if(data > 0){
                highlightRow("#8dc70a",column_name);
                resetTable();
            }else{
                highlightRow("#d64b2f",column_name);}
        });
}
function highlightRow(bgColor,text)
{
    var rowSelector = $("#txt_"+text);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function message(message, iconType)
{
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}
$(document).ready(function() {
    init();
    $('#txt_Birthday').datepicker({
        format: "dd/mm",
        startView: 1,
        maxViewMode: 1,
        language: "es",
        orientation: "top auto",
        autoclose: true
    });
});
// DELETE ------------------------------------------------------------------
$(document).on('click', '.DeletePerson', function(){
    deletePerson(path,$(this).data("id1"));
});
//GET --------------------------------------------------------------------- 
$(document).on('click', '.PersonInfo', function(){
    $('#btn_save').hide();
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'show_person',
            idPerson:$(this).data("id0")
        },  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            idPersonEdit = obj[0][0];
            $('#txt_legalName').val(obj[0][1]);
            $('#txt_namePerson').val(obj[0][2]);
            $('#txt_NIT').val(obj[0][3]);
            $('#txt_Phone').val(obj[0][4]);
            $('#txt_Birthday').val(obj[0][5]);
            editable = true;
        }  
   });
});
// SAVE NEW PART -----------------------------------------------------------
$(document).on('click', '#btn_save', function(){
    var legal = $('#txt_legalName').val();
    var name = $('#txt_namePerson').val();
    var nit = $('#txt_NIT').val();
    var phone = $('#txt_Phone').val();
    var birth = $('#txt_Birthday').val();
    if(name === '') {
        message("Debe ingresar un nombre para la persona.", "info");
        return;
    }
    if(phone === '') {
        message("Debe ingresar un número de telefono para la persona.", "info");
        return;
    }
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{ 
            type:'add_person',  
            legal:legal,
            name:name,
            nit:nit,
            phone:phone,
            birth:birth
        },  
        dataType:"text",  
        success:function(newId){
            if(newId > 0){
                message("La persona se ha guardado correctamente.", "success");
                clean();
                resetTable();
                var r = confirm("¿Desea ingresar un nuevo vehiculo?");
                if (r === true){ window.location="VehiclePerPerson.php"; }
            }else{
                message("Hubo un problema al guardar a la persona.", "error");
            }
        }
    });
});
// CANCEL EDIT PERSON --------------------------------------------------------    
$(document).on('click', '#btn_cancel', function(){
    $('#btn_save').show();
    clean();
    editable = false;
});
// EDIT PERSON  --------------------------------------------------------    
$(document).on('keyup', '.EditCompany', function(e){  
    editColumn(e, idPersonEdit, $("#txt_legalName").val(), "legalName");
});
$(document).on('keyup', '.EditName', function(e){  
    editColumn(e, idPersonEdit, $("#txt_namePerson").val(), "namePerson");
});
$(document).on('keyup', '.EditNIT', function(e){  
    editColumn(e, idPersonEdit, $("#txt_NIT").val(), "NIT");
});
$(document).on('keyup', '.EditTelephone', function(e){  
    editColumn(e, idPersonEdit, $("#txt_Telephone").val(), "Telephone");
});
$(document).on('keyup', '.EditBirthday', function(e){  
    editColumn(e, idPersonEdit, $("#txt_Birthday").val(), "Birthday");
});
