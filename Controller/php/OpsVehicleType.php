<?php    
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    include "../../Controller/php/general.php";
    $fns = new generalFunctions();    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    switch ($type_data)
    {
        case 'table_vehicle_type':
            $sql = "CALL catalog_list('VehicleType')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '';
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td contenteditable="true" class="EditVehicleType" style="width: 170%;" data-id1="'.$row["idCatalog"].'" id="'.$row["idCatalog"].'tdv">'.$row["nameCatalog"].'</td>
                            <td style="vertical-align: middle; width: 30%;  cursor:pointer;" class="DeleteVehicleType" data-id1="'.$row["idCatalog"].'">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </td>
                        </tr>';    
                }    
            }
            $output .= '
                <table class="table table-condensed table-hover table-bordered scroll" style="font-size:12px; text-align:center;" id="tableVehictleType">
                    <tbody>';
            $output .= $list;
            $output .= '
                    </tbody>
                    </table>';
        break;
        case 'vehicle_type_delete':
            $sql = "CALL catalog_delete('".$_POST['IdVehicleType']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'vehicle_type_edit':
            $sql = "CALL catalog_update('".$_POST['idVehicleType']."','".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'vehicle_type_add':
            $sql = "CALL catalog_add('".$_POST['nameVehicleType']."','VehicleType',@si)";
            $output = $sqlOps->sql_exec_op_return($sql);
        break;
    }
    echo $output == '' ? '' : $output;
