<?php
require('../../Tools/fpdf.php');

class PDF extends FPDF
{
    function Header()                               // Cabecera de página
    {
        $this->Image('../../Multimedia/Img/logo_pb.jpg',10,8,190);        // Logo (file,x,y,w,h,type,link)
        $this->Ln(35);
    }
    function Footer()                               // Pie de página     
    {
        $this->SetY(-15);                           // Posición: a 1,5 cm del final
        $this->SetFont('Arial','I',8);              // Arial italic 8
        $this->Cell(0,10,cosa('Página ').$this->PageNo().'/{nb}',0,0,'C');    // Número de página
    }
}
function cosa($text){
    return iconv('UTF-8', 'windows-1252', $text);
}
// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
session_start();
for($i=1;$i<40;$i++){
    $line = 'Imprimiendo línea número ñ '.$_SESSION['variable1'].$i;
    $pdf->Cell(0,10,cosa($line),0,1);
}
$pdf->Output('../../Reports/chuchote.pdf','F');


