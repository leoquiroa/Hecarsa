var path = "../Controller/php/OpsPart.php";
var categoryId = -1;
var idPartModified = -1;
var editable = false;

function ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init(){ 
    resetDdl('Categoria');
    resetTable();
    ajax_call(path,{type:'table_part_category'},"part_category_div");
}
function resetTable(){
    ajax_call(path,{type:'list_part'},"part_list_div");
    $('#dataTables-example').DataTable({
        responsive: true
    });
}
function resetDdl(title){
    ajax_call(path,{type:'category_ddl',title:title},"ddl_category_div");
    $('.selectpicker').selectpicker('show');
}
function clean(){
    $('#txt_name').val('');
    resetDdl('Categoria');
    $(".fileinput").fileinput("clear");
    $('#part_preview').attr("src","../Multimedia/Parts/no_image.png");
    idPartModified = -1;
}
// SAVE
function saveImageInServer(newId,path){
    var formData = new FormData();
    formData.append('file', $("#new_photo_form")[0].files[0]);
    var response = '';
    var name = newId + new Date().getMilliseconds();
    $.ajax({
        url: '../Controller/php/uploadImage.php?var='+name+'&url=../../Multimedia/Parts/',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        async:false,
        success: function (r) {
            if(r === null){
                message("Hubo un problema interno del servidor al guardar la parte.", "error");
                deletePart(path,'remove_part_db',newId);
            }else{
                var res = r.split("#");
                if(res[0]>0){
                    message("La imagen de la parte fue guardada exitosamente en el servidor.", "success");
                    response = '1#'+res[1]+'#'+name;
                }
                else{
                    message("Hubo un problema al guardar la imagen de la parte en el servidor. "+res[1], "error");
                    deletePart(path,'remove_part_db',newId);
                }
            }
        }
    });
    return response;
}
function updateImageSourceDB(ext,path,newId,name){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{ 
            type:'update_part', 
            id:newId, 
            column_name:'imageUrl', 
            texto:name+'.'+ext},  
        dataType:"text",  
        success:function(Id){
            if(Id > 0){
                message("La asociación de la imagen con la base datos fue exitosa.", "success");
            }else{
                message("Hubo un problema al asociar la imagen con la base datos.", "error");
                deletePart(path,newId);
            }
        }
    });
}
// DELETE
function deletePartCalls(path,type,idPart){
    var response = -1;
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{ 
            type:type,
            idPart:idPart},  
        dataType:"text",
        success:function(Id){
            if(Id > 0){
                if(type === 'remove_part_file')
                    message("La imagen de la parte se ha eliminado.", "warn");
                else if(type === 'remove_part_db')
                    message("La parte se ha eliminado de la base de datos.", "warn");
                response = 1;
            }else{
                if(type === 'remove_part_file')
                    message("Hubo un problema al eliminar la imagen de la parte.", "error");
                else if(type === 'remove_part_db')
                    message("Hubo un problema al eliminar la parte de la base de datos.", "error");
                response = 0;
            }
        }
    });
    return response;
}
function deletePart(path,idPart){
    deletePartCalls(path,'remove_part_file',idPart);
    if(deletePartCalls(path,'remove_part_db',idPart)){
        resetTable();
    }
}
// DISABLE ENTER
function disableEnter(e){
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
}
// EDIT NAME
function editColumn(e,dataNo,newValue,colName,objectToUpdate){
    if(e.keyCode == 13)
    {          
       edit_data(dataNo,newValue,colName,objectToUpdate);
    }
}
function edit_data(id,texto,column_name,objectToUpdate){   
    if(objectToUpdate === 'part'){
        $.post( path, { 
            type:'update_part', 
            id: id, 
            column_name:column_name, 
            texto:texto
        }).done(function(data) 
        {
            if(data > 0){
                highlightRow("txt_name","#8dc70a");
                resetTable();
            }else{
                highlightRow("txt_name","#d64b2f");}
        });
    }else if(objectToUpdate === 'category'){
        $.post( path, { 
            type:'update_part_category', 
            idPartCategory: id, 
            texto:texto
        }).done(function(data) 
        {
            if(data > 0){
                highlightRow(id + "cdp","#8dc70a");
                resetTable();
            }else{
                highlightRow(id + "cdp","#d64b2f");}
        });
    }
}
function highlightRow(objId,bgColor){
    var rowSelector = $("#" + objId);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function message(message, iconType){
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}

$(document).ready(function() {
    init();
});
// DROP DOWN LISTS ---------------------------------------------------------
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDdl = this.id;
    var selected = $(this).find("option:selected").val();
    if(nameDdl === 'ddl_part_category'){
        categoryId = selected;
        if(editable){
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{ 
                    type:'update_part',
                    id:idPartModified,
                    column_name:'categoryPart',
                    texto:categoryId
                },dataType:"text",
                success:function(Id){
                    if(Id > 0){
                        message("La categoria de la parte se actualizó correctamente.", "success");
                        resetTable();
                    }else{
                        message("Hubo un problema al actualizar la categoria de la parte.", "error");
                    }
                }
            });
        }
    }
});
// PART PIC --------------------------------------------------------    
$(document).on('click', '#update_part_pic', function(){
    if(idPartModified>0){
        var r = saveImageInServer(idPartModified,path);
        var res = r.split("#");
        if(res[0]>0){
            updateImageSourceDB(res[1],path,idPartModified,res[2]);
            resetTable();
            clean();
        }
    }
});
// PART  --------------------------------------------------------    
$(document).on('click', '.PartName', function(){
    $('#btn_save').hide();
    var idPart = $(this).data("id0");
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'show_part',
            idPart:idPart
        },  
        dataType:"text",  
        success:function(d){
            clean();
            var obj = JSON.parse(d);
            idPartModified = obj[0][0];
            $('#txt_name').val(obj[0][1]);
            resetDdl(obj[0][2]);
            $('#part_preview').attr("src","../Multimedia/Parts/"+obj[0][3]);
            editable = true;
        }  
   });
});
$(document).on('keyup', '.EditPartName', function(e){  
    editColumn(e, idPartModified, $("#txt_name").val(), "namePart", "part");
});
$(document).on('keydown', '.EditPartName', function(e){
    disableEnter(e);
});
$(document).on('click', '.DeletePart', function(){
    var idPart = $(this).data("id1");
    deletePart(path,idPart);
});
// PART BUTTONS --------------------------------------------------------    
$(document).on('click', '#btn_save_part', function(){
    var name = $('#txt_name').val();
    if(name === '') {
        message("Debe ingresar un nombre para la parte.", "info");
        return;
    }
    if(categoryId < 0) {
        message("Debe seleccionar una categoria para la parte.", "info");
        return;
    }
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{ 
            type:'add_part',  
            name:name, 
            categoryId: categoryId
        },  
        dataType:"text",  
        success:function(newId){
            if(newId > 0){
                message("La parte se ha guardado correctamente en la base de datos", "success");
                if($("#new_photo_form").val()!==''){
                    var r = saveImageInServer(newId,path);
                    var res = r.split("#");
                    if(res[0]>0){
                        updateImageSourceDB(res[1],path,newId,res[2]);
                    }
                }
                clean();
                resetTable();
            }else{
                message("Hubo un problema al guardar la parte en la base de datos.", "error");
            }
        }
    });
});
$(document).on('click', '#btn_cancel_part', function(){
    $('#btn_save').show();
    clean();
    editable = false;
});
// CATEGORY 
$(document).on("click",".DeleteCategoryPart", function () {
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'category_part_delete',
            IdCategoryPart:$(this).data("id1")
        },  
        dataType:"text",  
        success:function(d){  
            if(parseInt(d)>0){
                message("La categoria de la parte se ha eliminado correctamente.", "warn");
                init();
            }
            else
                message("Hubo un problema al eliminar la categoria de la parte.", "error");
        }  
    });
});
$(document).on('keyup', '.EditCategoryPart', function(e){
    editColumn(e, $(this).data("id1"), $(this).text(), "", "category");
});
$(document).on('keydown', '.EditCategoryPart', function(e){
    disableEnter(e);
});    
$(document).on("click","#btn_save_category", function () {
        var name = $('#newPartCategory').val();
        if(name === '') {
            message("Debe ingresar el nombre de la nueva categoria de parte.", "info");
            return;
        }
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'part_category_add',
                nameCategoryPart:name
            },  
            dataType:"text",  
            success:function(newId){
                if(newId > 0){
                    message("La nueva categoria de parte se ha guardado correctamente.", "success");
                    $('#newPartCategory').val('');
                    init();
                }
                else
                {
                    message("Hubo un problema al guardar la nueva categoria de parte.","error");
                }
            }
        });
});
$(document).on('click', '#btn_cancel_category', function(e){
    $('#newPartCategory').val('');
});