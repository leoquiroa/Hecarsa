var path = "../Controller/php/OpsVehicleType.php";
var IdVehicleType = -1;
var aliens = true;

function ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init(){ 
    ajax_call(path,{type:'table_vehicle_type'},"table_vehicle_type_div");
}
function disableEnter(e){
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
}
function editColumn(e, dataNo, newValue){
    if(e.keyCode == 13)
    {          
       edit_data(dataNo,newValue);
    }
}
function edit_data(id, texto){   
    $.post( path, { type:'vehicle_type_edit', idVehicleType: id, texto:texto } )
        .done(function(data) 
        {
            if(data > 0)
            {
                highlightRow(id, "#8dc70a");
            }
            else
                highlightRow(id, "#d64b2f");
        });
}
function highlightRow(rowId, bgColor){
    var rowSelector = $("#" + rowId + "tdv");
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function message(message, iconType)
{
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}

$(document).ready(function() {
    init(); 
});
$(document).on("click",".DeleteVehicleType", function () {
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'vehicle_type_delete',
            IdVehicleType:$(this).data("id1")
        },  
        dataType:"text",  
        success:function(d){  
            if(parseInt(d)>0){
                message("El tipo de vehiculo se ha eliminado correctamente.", "warn");
                init();
            }
            else
                message("Hubo un problema al eliminar el tipo de vehiculo.", "error");
        }  
    });
});
$(document).on("click","#btn_save_tdv", function () {
        var name = $('#newVehicleType').val();
        if(name === '') {
            message("Debe ingresar el nombre del tipo de vehiculo.", "info");
            return;
        }
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'vehicle_type_add',
                nameVehicleType:name
            },  
            dataType:"text",  
            success:function(newId){
                if(newId > 0){
                    message("El tipo de vehiculo se ha guardado correctamente.", "success");
                    $('#newVehicleType').val('');
                    init();
                }
                else
                {
                    message("Hubo un problema al guardar el tipo de vehiculo.","error");
                }
            }
        });
});
$(document).on('click', '#btn_cancel_tdv', function(e){
    $('#newVehicleType').val('');
});
$(document).on('keyup', '.EditVehicleType', function(e){  
    editColumn(e, $(this).data("id1"), $(this).text());
});
$(document).on('keydown', '.EditVehicleType', function(e){
    disableEnter(e);
});    
