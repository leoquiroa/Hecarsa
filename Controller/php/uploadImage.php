<?php
    if (isset($_FILES["file"]["type"])){
        $validextensions = array("image/jpeg","image/jpg","image/png");
	$temporary = explode(".", $_FILES["file"]["name"]);
	$file_extension = end($temporary);
        $file_type = $_FILES["file"]["type"];
	if (!in_array($file_type, $validextensions)){
            echo "0#Tipo de fotografía invalida. Validas: jpg, jpeg o png.";}
        if ($_FILES["file"]["size"] > 1000000){ //Approx. 100kb files can be uploaded.
            echo "0#Tamaño invalido del archivo. Debe ser menor a 1 MB";}
        if ($_FILES["file"]["error"] > 0){
            echo "0#Archivo con errores: " . $_FILES["file"]["error"];}
        else{
            $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
            $targetPath = $_REQUEST["url"] . $_REQUEST["var"] . "." . $file_extension; // Target path where file is to be stored
            echo move_uploaded_file($sourcePath, $targetPath) ? '1#'.$file_extension : '0#Problemas en el servidor al guardar la fotografia.'; // Moving Uploaded file
        }
    }
?>
