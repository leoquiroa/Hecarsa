<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="description" content="Hecarsa Taller site">
        <meta name="author" content="@leoquiroa">
        <title>Servicios por Persona</title>
        <!--Third CSS-->
        <link href="../Controller/css/External/bootstrap.3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/bootstrap-select.1.12.0.min.css" rel="stylesheet" type="text/css"/>
        <!--Own CSS-->
        <link href="../Controller/css/External/autocomplete.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/service.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/verticalScroll.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="service.php">Hecarsa</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <!-- User -->
                <li class="active">
                    <a href="#" data-toggle="collapse" data-target="#demoHome">
                        <i class="fa fa-object-ungroup"></i> Inicio <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoHome" class="collapse">
                        <li>
                            <a href="service.php"><i class="fa fa-home"></i> Servicio </a>
                            <a href="serviceLog.php"><i class="fa fa-list"></i> Historial </a>
                            <a href="calendar.php"><i class="fa fa-list"></i> Calendario </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoElements">
                        <i class="fa fa-object-group"></i> Elementos <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoElements" class="collapse">
                        <li>
                            <a href="part.php"><i class="fa fa-anchor"></i> Partes </a>
                            <a href="person.php"><i class="fa fa-users"></i> Personas </a>
                            <a href="VehiclePerPerson.php"><i class="fa fa-car"></i> Vehículos por Persona</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoConfigurations">
                        <i class="fa fa-cogs"></i> Configuraciones <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoConfigurations" class="collapse">
                        <li>
                            <a href="serviceType.php"><i class="fa fa-sellsy"></i> Tipos de Servicios </a>
                            <a href="PartPerServiceType.php"><i class="fa fa-xing-square"></i> Partes por Tipos de Servicio </a>
                            <a href="VehicleType.php"><i class="fa fa-angellist"></i> Tipos de Vehículos </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- ################################################# MENU ################################################# -->       
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">   
                <div style="font-size: 24px; text-align: center; color: #d9534f;">
                    <i class="fa fa-key"></i> HISTORIAL DE SERVICIOS
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <input type="text" class="form-control" id="searchByName" style="width: 100%" placeholder="Nombre de la persona">
                    </div>
                    <div class="col-md-1" style="text-align: center;">ó</div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="searchByPlate" style="width: 100%" placeholder="Número de placa">
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="personHeadDiv"></div>
                        <div id="vehicleTableDiv"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="vehicleHeadDiv"></div>
                        <div id="serviceTableDiv"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="serviceHeadDiv"></div>
                        <div id="partTableDiv"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div id="dummy_div"></div>
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
<!--                        <button class="btn btn-info" id="print" style="width: 100%;">
                            Generar Reporte
                        </button>-->
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>
            <div id="alertForm" class="form-group" style="text-align: center;">
                <textarea disabled id="alertText" placeholder="Razón" class="form-control nuevo" rows="4"></textarea>
                <br/>
                <button class="btn btn-info" id="deactivateUser">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                </button>
            </div>
        </div>  
    </div>
    <!--Third JS-->
    <script src="../Controller/js/External/jquery.3.1.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap.3.3.7.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/jquery-ui.1.12.1.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap-select.1.12.0.min.js" type="text/javascript"></script>
    <!--Own JS-->
    <script src="../Controller/js/serviceLog.js" type="text/javascript"></script>
    </body>
</html>                    