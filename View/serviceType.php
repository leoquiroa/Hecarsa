<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="description" content="Hecarsa Taller site">
        <meta name="author" content="@leoquiroa">
        <title>Tipos de Servicios</title>
        <!--Third CSS-->
        <link href="../Controller/css/External/bootstrap.3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/bootstrap-select.1.12.0.min.css" rel="stylesheet" type="text/css"/>
        <!--Own CSS-->
        <link href="../Controller/css/part.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="service.php">Hecarsa</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <!-- User -->
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoHome">
                        <i class="fa fa-object-ungroup"></i> Inicio <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoHome" class="collapse">
                        <li>
                            <a href="service.php"><i class="fa fa-home"></i> Servicio </a>
                            <a href="serviceLog.php"><i class="fa fa-list"></i> Historial </a>
                            <a href="calendar.php"><i class="fa fa-list"></i> Calendario </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoElements">
                        <i class="fa fa-object-group"></i> Elementos <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoElements" class="collapse">
                        <li>
                            <a href="part.php"><i class="fa fa-anchor"></i> Partes </a>
                            <a href="person.php"><i class="fa fa-users"></i> Personas </a>
                            <a href="VehiclePerPerson.php"><i class="fa fa-car"></i> Vehículos por Persona</a>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="#" data-toggle="collapse" data-target="#demoConfigurations">
                        <i class="fa fa-cogs"></i> Configuraciones <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoConfigurations" class="collapse">
                        <li>
                            <a href="serviceType.php"><i class="fa fa-sellsy"></i> Tipos de Servicios </a>
                            <a href="PartPerServiceType.php"><i class="fa fa-xing-square"></i> Partes por Tipos de Servicio </a>
                            <a href="VehicleType.php"><i class="fa fa-angellist"></i> Tipos de Vehículos </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- ################################################# MENU ################################################# -->       
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">   
                <div style="font-size: 24px; text-align: center; color: #d9534f;">
                    <i class="fa fa-industry"></i> TIPOS DE SERVICIOS
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/checklist_256.png" alt="" style="width: 25px; height: 25px;">
                            <strong>LISTADO DE TIPOS DE SERVICIOS</strong>
                        </div>
                        <br/>
                        <div id="part_list_div"></div>      
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="service_type_div"></div>   
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/parts-512.png" alt="" style="width: 25px; height: 25px;">
                            <strong>DETALLE DE TIPOS DE SERVICIOS</strong>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-2">
                        <span  style="display: inline-block; height: 34px; ">Tipo de Servicio</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Tiempo</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Odometro</span><br/>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control EditServiceType" id="txt_NameServiceType" placeholder="Tipo de Servicio">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control EditTime" id="txt_Time" placeholder="Tiempo">
                            </div>
                            <div class="col-md-6">
                                <div id="catalogUnitTime"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control EditOdometer" id="txt_Odometer" placeholder="Odometro">
                            </div>
                            <div class="col-md-6">
                                <div id="catalogOdometer"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-2">
                        <button class="btn btn-block btn-success" type="button" id="btn_save">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-block btn-danger" type="button" id="btn_cancel">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-block btn-danger" type="button" id="btn_delete">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br/>
            </div>  
        </div>  
    </div>
    <!--Third JS-->
    <script src="../Controller/js/External/jquery.3.1.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap.3.3.7.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/jquery.dataTables.1.10.12.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/dataTables.responsive.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap-select.1.12.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <!--Own JS-->
    <script src="../Controller/js/servicetype.js" type="text/javascript"></script>
    </body>
</html>                    