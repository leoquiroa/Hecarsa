//server paths
var pathAutoComplete = "../Controller/php/OpsAutoCompleteIndex.php";
var pathIndexOps = "../Controller/php/OpsService.php";

var gbl_idVehicle = -1;
var gbl_idService = -1;
var OdometerType = '';
var partCheck = [];
var gbl_Parts = []; 
var gbl_Services = [];
var bigTotal = 0;

// general /////////////////////////////////////////////////////////////////////
function ajax_call(sent_url,sent_data,div,async_){
    $.ajax({
        cache: false,
        async: async_,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function diffArray(array1, array2){
    return array1.filter(
        function(x) { 
            return array2.indexOf(x) < 0; 
    });
}
function findInObj(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr].toString() === value.toString()) {
            return i;
        }
    }
    return -1;
}
function message(message, iconType)
{
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}
// get check parts from "on screen" service ////////////////////////////////////
function getSavedObjs(array_,column,idService){
    var savedParts = [];
    for (var j = 0;j < array_.length; j++) {
        if(array_[j]["Servicio"] === idService){
            savedParts.push(array_[j][column]);
        }
    }
    return savedParts;
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function isValidPrice(number){
    var money = /^(\Q?\d{1,3}(,?\d{3})*(\.\d{1,2})?)$/;
    return money.test(number);
}
// visibility //////////////////////////////////////////////////////////////////
function labelVisibility(status){
    document.getElementById("lbl_currentOdometer").style.visibility = status;
    document.getElementById("lbl_nextOdometer").style.visibility = status;
    document.getElementById("lbl_currentDate").style.visibility = status;
    document.getElementById("lbl_nextDate").style.visibility = status;
    document.getElementById("lbl_Note").style.visibility = status;
    document.getElementById("lbl_Alert").style.visibility = status;
}
function textVisibility(status){
    document.getElementById("txt_currentOdometer").style.visibility = status;
    document.getElementById("txt_nextOdometer").style.visibility = status;
    document.getElementById("txt_currentDate").style.visibility = status;
    document.getElementById("txt_nextDate").style.visibility = status;
    document.getElementById("txt_Note").style.visibility = status;
    document.getElementById("txt_Alert").style.visibility = status;
}
function cleanText(){
    document.getElementById("txt_currentOdometer").value = '';
    document.getElementById("txt_nextOdometer").value = '';
    document.getElementById("txt_currentDate").value = '';
    document.getElementById("txt_nextDate").value = '';
    document.getElementById("txt_Note").value = '';
    document.getElementById("txt_Alert").value = '';
}
function btnSaveServiceVisibility(status){
    document.getElementById("btnSaveService").style.display = status;
}
// autocomplete ////////////////////////////////////////////////////////////////
$(document).ready(function() {
    autoCompleteByName();
    autoCompleteByPlate();
    labelVisibility("hidden");
    textVisibility("hidden");
    btnSaveServiceVisibility("none");
});
function autoCompleteByName(){
    $("#searchByName").autocomplete({
        source: function( request, response ) {
            $.ajax({
                dataType: "JSON",
                url: pathAutoComplete,
                data: {
                    type:'ByName'
                    ,term: request.term
                },
                success: function(d){  
                    response(d);
                },
                error: function (d) {
                    redirect('searchByName');
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#searchByPlate').val('');
            fillVehicleTable(ui.item.id);
        }
    });
}
function autoCompleteByPlate(){
    $("#searchByPlate").autocomplete({
        source: function( request, response ) {
            $.ajax({
                dataType: "JSON",
                url: pathAutoComplete,
                data: {
                    type:'ByPlate'
                    ,term: request.term
                },
                success: function(d){  
                    response(d);
                },
                error: function (d) {
                    redirect('searchByPlate');
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#searchByName').val('');
            fillVehicleTable(ui.item.id);
        }
    });
}
function fillVehicleTable(id){
    cleanTableVehicles();
    cleanTableServices();
    ajax_call(pathIndexOps,
        {type:'PersonHead'
        ,idPerson:id
    },"person_head_div",true);
    ajax_call(pathIndexOps,
        {type:'VehiclesTable'
        ,idPerson:id
    },"vehicle_table_div",true);
    cleanVisualService();
}
function redirect(nameField){
    var r = confirm("¿Desea ingresar un nuevo cliente?");
    if (r===true){ window.location="person.php"; }
    $('#' + nameField).val('');
 }
// clean ///////////////////////////////////////////////////////////////////////
function cleanTableVehicles(){
    $('#person_head_div').html('');
    $('#vehicle_table_div').html('');
}
function cleanTableServices(){
    $('#vehicle_head_div').html('');
    $('#old_service_table_div').html('');
}
function cleanServiceCheck(){
    $('#ddl_servicetype_div').html('');
    $('#part_check_div').html('');
}
function cleanCurrentService(){
    $('#part_newService_div').html('');
    $('#nameService').text('');
    labelVisibility("hidden");
    textVisibility("hidden");
    cleanText();
    $('#ddl_odometer_div').html('');
}
function cleanSummaryService(){
    $('#summary_service_div').html('');
    $("#bigTotal").text('');
    btnSaveServiceVisibility("none");
}
// see the log service of a vehicle ////////////////////////////////////////////
$(document).on('click', '.vehicleList', function(){
    cleanTableServices();
    cleanVisualService();
    cleanGlobalVariables();
    gbl_idVehicle = $(this).data("id0");
    fillServiceTable();
});
function fillServiceTable(){
     ajax_call(pathIndexOps,
        {type:'VehiclesHead'
        ,idVehicle:gbl_idVehicle
    },"vehicle_head_div",true);
    ajax_call(pathIndexOps,
        {type:'ServicesTable'
        ,idVehicle:gbl_idVehicle
    },"old_service_table_div",true);
    ajax_call(pathIndexOps,{
        type:'DdlServiceType'
        ,title:'Tipo de Servicio'
    },"ddl_servicetype_div",false);
    $('.selectpicker').selectpicker('show');
}
//drop down list to 
//select a new service
//select a odometer type
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDDL = this.id;
    var selected = $(this).find("option:selected").val();
    if(nameDDL === 'ddl_service_type'){
        gbl_idService = selected;
        partCheck = getSavedObjs(gbl_Parts,"Parte",selected);
        ajax_call(pathIndexOps,{
            type:'PartPerServiceTableCheck'
            ,idNewService:selected
            ,checkList:partCheck
        },"part_check_div",true);
    }
    if(nameDDL === 'ddl_odometer'){
        OdometerType = selected;
        var ixS = findInObj(gbl_Services, "idServicio", gbl_idService);
        for (var m = 0; m < gbl_Services.length; m++) {
            gbl_Services[m]["TipoOdometro"] = OdometerType;
        }
        updateOdometerField($('#txt_currentOdometer').val(),ixS);
    }
});
// check the parts on the table ////////////////////////////////////////////////
$(document).on('click', '.classCheckPart', function(e){
    var newPart = $(this).val();
    var columnCheck = document.getElementsByClassName("classCheckPart");
    //ALL ELEMENTS
    if(parseInt(newPart) === 0){
        if(Boolean($('#mainCheck')[0].checked)){
            $(".classCheckPart").prop("checked", true); //graphically
            partCheck.length = 0;
            for (var index = 1; index < columnCheck.length; index++) {
                addPart(partCheck,columnCheck[index].value);
            }
        }
        else{
            cleanAllCheck(partCheck);
            mainOperation();
        }
    }//ONE ELEMENT
    else{
        addPart(partCheck,newPart);
        $("#mainCheck").prop("checked", 
            ((columnCheck.length - 1) === partCheck.length));
    }
    activateDatePicker();
});
function activateDatePicker(){
    $('#txt_currentDate').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    $('#txt_nextDate').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
}
function cleanAllCheck(_array){
    $(".classCheckPart").prop("checked", false);
    _array.length = 0;
}
function addPart(_array,newPart){
    var ix = _array.indexOf(newPart);
    if(ix > -1){ //EXIST -> REMOVE
        _array.splice(ix, 1);
    }
    else{ //NEW -> ADD
        _array.push(newPart);
    }
    mainOperation();
}
function mainOperation(){
    //save-old-table
    saveOldParts();
    //save-new-memory
    saveNewPartAndService();
    //set-new-table
    ajax_call(pathIndexOps,{
        type:'currentServiceTable'
        ,memoryParts:gbl_Parts
    },"part_newService_div",true);
}
function saveOldParts(){
    var jsTable = document.getElementById("tableNewService");
    if(jsTable !== null){
        var NoRows = jsTable.rows.length;
        for (var i = 1; i < NoRows; i++) {
            var fila = jsTable.rows[i];
            gbl_Parts[i-1]["Codigo"] = fila.cells[3].innerHTML.replace(/\<br\>/g, "");
            gbl_Parts[i-1]["Marca"] = fila.cells[4].innerHTML.replace(/\<br\>/g, "");
            gbl_Parts[i-1]["Precio"] = fila.cells[5].innerHTML.replace(/\<br\>/g, "");
            gbl_Parts[i-1]["Nota"] = fila.cells[6].innerHTML.replace(/\<br\>/g, "");
        }
    }
}
function saveNewPartAndService(){
    var IsEmpty = syncMemoryParts();
    var ixS = findInObj(gbl_Services, "idServicio", gbl_idService);
    if(ixS<0 & !IsEmpty){ //is new service
        saveOldServices();
        saveNewService(gbl_idService);        
        fillDdlOdometer(' ');
        labelVisibility("visible");
        textVisibility("visible");
    }
}
// sync memory parts ///////////////////////////////////////////////////////////
function syncMemoryParts(){
    var savedParts = getSavedObjs(gbl_Parts,"Parte",gbl_idService);
    //delete
    var toRemove = diffArray(savedParts, partCheck);
    var IsEmpty = removeMemoryParts(toRemove,gbl_idService);
    //save
    var toAdd = diffArray(partCheck, savedParts);
    fillmemoryParts(toAdd);
    return IsEmpty;
}
function removeMemoryParts(arrayToDelete,index){
    var NoPartsToDelete = arrayToDelete.length;
    if(NoPartsToDelete>0){
        //parts
        var k=0;
        do{
            if(gbl_Parts[k]["Servicio"] === index){
                var ix = arrayToDelete.indexOf(gbl_Parts[k]["Parte"]);       
                if(ix > -1){
                    gbl_Parts.splice(k, 1);
                    NoPartsToDelete--;
                    k = 0;
                }
            }
            k = (gbl_Parts.length - 1) > k ? (k + 1) : 0;
        }while(NoPartsToDelete > 0);
        return removeMemoryServices(index);
    }
    return false;
}
function removeMemoryServices(index){
    //services
    var ixP = findInObj(gbl_Parts, "Servicio", index);
    if(ixP<0){
        var ixS = findInObj(gbl_Services, "idServicio", index);
        gbl_Services.splice(ixS, 1);

        if(gbl_Services.length > 0){
            getNewServiceInfo(gbl_Services[0]["idServicio"]);
            updatePricesTable();
            updateBigTotal();
        }else{
            cleanCurrentService();
            cleanSummaryService();
            gbl_idService = -1;
            return true;
        }
    }else{
        var partialTotal = calculateTotalPerService(index,-1,0);
        updateAllPrices(index,partialTotal);
    }
    return false;
}
function fillmemoryParts(arrayToAdd){
    for (var i = 0; i < arrayToAdd.length; i++) {
        var obj = {
            'Servicio' : '',
            'Parte' : '',
            'Codigo' : '',
            'Marca' : '',
            'Precio' : '',
            'Nota' : ''
        };
        obj.Servicio = gbl_idService;
        obj.Parte = arrayToAdd[i];
        gbl_Parts.push(obj); 
    }
}
// save a new service //////////////////////////////////////////////////////////
function saveNewService(idService){
    $.ajax({
        cache: false,
        async: false,
        url:pathIndexOps,  
        method:"POST",
        data:{
            type:'getServiceName',
            idNewService:idService
        },  
        dataType:"text",  
        success:function(d){  
            if(d !== ''){
                var obj = JSON.parse(d);
                fillMemoryServices(idService, obj);
                $('#nameService').text(obj[0]);
                updatePricesTable();
            }
        }  
    });
}
function fillMemoryServices(idService, obj){
    $('#txt_currentDate').val(obj[1]);
    $('#txt_nextDate').val(obj[2]);
    $('#txt_currentOdometer').val('');
    $('#txt_nextOdometer').val('');
    
    var obj1 = {
        'idServicio' : idService,
        'nombreServicio' : obj[0],
        'OdometroAhora' : '',
        'OdometroSiguiente' : '',
        'TipoOdometro' : '',
        'OdometroMi' : obj[4],
        'OdometroKm' : obj[3],
        'FechaAhora' : obj[1],
        'FechaSiguiente' : obj[2],
        'Costo' : '',
        'Nota' : '',
        'Alerta' : ''
    };
    gbl_Services.push(obj1); 
}
// set a price to the a part ///////////////////////////////////////////////////
$(document).on('keyup', '.newPricePart', function(e){
    var partPrice = $(this).text();
    if(isValidPrice(partPrice)){
        var splitted = $(this).data("id0").split("%");
        var idService = splitted[0];
        var idPart = splitted[1];
        var partialTotal = calculateTotalPerService(idService,idPart,partPrice);
        updateAllPrices(idService,partialTotal);
    }
});
// price ///////////////////////////////////////////////////////////////////////
function updateAllPrices(idService,partialTotal){
    var ixS = findInObj(gbl_Services, "idServicio", idService);
    gbl_Services[ixS]["Costo"] = partialTotal.toFixed(2);
    updatePricesTable();
    updateBigTotal();
}
function calculateTotalPerService(idService,idPart,partPrice){
    var partialTotal = parseFloat(partPrice);
    for (var j = 0;j < gbl_Parts.length; j++) {
        if(gbl_Parts[j]["Servicio"] === idService){
            if(gbl_Parts[j]["Parte"] === idPart)
                gbl_Parts[j]["Precio"] = partPrice;
            else
                partialTotal += parseFloat(gbl_Parts[j]["Precio"]) || 0;
        }
    }
    return partialTotal;
}
function updatePricesTable(){
    ajax_call(pathIndexOps,
        {type:'summaryPrices'
        ,memoryServices:gbl_Services
    },"summary_service_div",true);
}
function updateBigTotal(){
    var total = 0;
    for(var p = 0; p < gbl_Services.length; p += 1) {
        total += parseFloat(gbl_Services[p]["Costo"]) || 0;
    }
    bigTotal = total;
    $("#bigTotal").text("TOTAL: Q " + total.toFixed(2));
    btnSaveServiceVisibility("block");
}
// odometer ////////////////////////////////////////////////////////////////////
$(document).on('keyup', '.newCurrentOdometer', function(e){
    var ixS = findInObj(gbl_Services, "idServicio", gbl_idService);
    updateOdometerField($(this).val(),ixS);
});
function updateOdometerField(currentOdometer,ixS){
    var finalOdometer = 0;
    if(isNumber(currentOdometer)){
        for (var m = 0; m < gbl_Services.length; m++) {
            finalOdometer = parseInt(currentOdometer);
            gbl_Services[m]["OdometroAhora"] = finalOdometer;
            if(OdometerType === 'Km'){
                finalOdometer += parseInt(gbl_Services[m]["OdometroKm"]);
            }else if(OdometerType === 'Mi'){
                finalOdometer += parseInt(gbl_Services[m]["OdometroMi"]);
            }
            gbl_Services[m]["OdometroSiguiente"] = finalOdometer;
        }
    }else{
        $('#txt_nextOdometer').val('');
        gbl_Services[ixS]["OdometroAhora"] = finalOdometer;
    }
    $('#txt_nextOdometer').val(gbl_Services[ixS]["OdometroSiguiente"]);
}
// date ////////////////////////////////////////////////////////////////////
$(document).on('changeDate', '#txt_currentDate', function(e){
    var ixS = findInObj(gbl_Services, "idServicio", gbl_idService);
    gbl_Services[ixS]["FechaAhora"] = calculateDate(e);
});
$(document).on('changeDate', '#txt_nextDate', function(e){
    var ixS = findInObj(gbl_Services, "idServicio", gbl_idService);
    gbl_Services[ixS]["FechaSiguiente"] = calculateDate(e);
});
function calculateDate(ev){
    var fecha = ev.date;
    var dd = fecha.getUTCDate();
    var mm = (fecha.getUTCMonth()+1);
    var yyyy = fecha.getUTCFullYear();
    if(dd<10) { dd='0'+ dd; }
    if(mm<10) { mm='0'+ mm; }
    return dd + '-' + mm + '-' + yyyy;
}
// retrieval service ///////////////////////////////////////////////////////////
$(document).on('click', '.GeneralService', function(e){
    saveOldServices();
    gbl_idService = $(this).data("id0");
    getNewServiceInfo(gbl_idService);
});
function saveOldServices(){
    var ixOld = findInObj(gbl_Services, "idServicio", gbl_idService);
    if(ixOld > -1){
        gbl_Services[ixOld]["OdometroAhora"] = $('#txt_currentOdometer').val().replace(/\<br\>/g, "");
        gbl_Services[ixOld]["Nota"] = $('#txt_Note').val().replace(/\<br\>/g, "");
        gbl_Services[ixOld]["Alerta"] = $('#txt_Alert').val().replace(/\<br\>/g, "");
    }
}
function getNewServiceInfo(idnew){
    var ixSee = findInObj(gbl_Services, "idServicio", idnew);
    if(ixSee > -1){
        $('#nameService').text(gbl_Services[ixSee]["nombreServicio"]);
        gbl_idService = gbl_Services[ixSee]["idServicio"];
        $('#txt_currentOdometer').val(gbl_Services[ixSee]["OdometroAhora"]);
        $('#txt_nextOdometer').val(gbl_Services[ixSee]["OdometroSiguiente"]);
        OdometerType = gbl_Services[ixSee]["TipoOdometro"];
        fillDdlOdometer(OdometerType);
        $('#txt_currentDate').val(gbl_Services[ixSee]["FechaAhora"]);
        $('#txt_nextDate').val(gbl_Services[ixSee]["FechaSiguiente"]);
        $('#txt_Note').val(gbl_Services[ixSee]["Nota"]);
        $('#txt_Alert').val(gbl_Services[ixSee]["Alerta"]);
    }
}
function fillDdlOdometer(title){
    ajax_call(pathIndexOps,{type:'DdlOdometerType',title:title},"ddl_odometer_div",false);
    $('.selectpicker').selectpicker('show');
}
// save a new service //////////////////////////////////////////////////////////
$(document).on('click', '#btnSaveService', function(){
    if(gbl_Services.length < 1) {message("No ha seleccionado ningún servicio.", "warn"); return;}
    if(gbl_Parts.length < 1) {message("No ha seleccionado ninguna parte.", "warn"); return;}
    if(gbl_idVehicle < 1) {message("No ha seleccionado ningún vehiculo.", "warn"); return;};
    if(bigTotal < 1) {message("No ha ingresado un precio.", "warn"); return;};
    if(OdometerType === '') {message("No ha seleccionado el tipo de odometro.", "warn"); return;};
    
    saveOldParts();
    saveOldServices();
    $.ajax({
        cache: false,
        async: false,
        url:pathIndexOps,  
        method:"POST",
        data:{
            type:'saveServices'
            ,memoryServices:gbl_Services
            ,memoryParts:gbl_Parts
            ,idVehicle:gbl_idVehicle
            ,totalCost:bigTotal
        },  
        dataType:"text",  
        success:function(d){
            if(d>0){
                cleanVisualService();
                cleanGlobalVariables();
                ajax_call(pathIndexOps,
                    {type:'ServicesTable'
                    ,idVehicle:gbl_idVehicle
                },"old_service_table_div",true);
                fillServiceTable();
                //gbl_idVehicle = -1;
                message("Visita guardada con exito", "success");
            }else{
                message("Hubo un problema al guardar la visita.", "error");
            }
        }  
    });
});
function cleanGlobalVariables(){
    gbl_idService = -1;
    OdometerType = '';
    partCheck = [];
    gbl_Parts = []; 
    gbl_Services = [];
    bigTotal = 0;
}
function cleanVisualService(){
    cleanServiceCheck();
    cleanCurrentService();
    cleanSummaryService();   
    
}

/*
    var texto = "\n";
    for(var m = 0; m < gbl_Parts.length; m++)
        texto += 
        gbl_Parts[m]["Servicio"] + "," +
        gbl_Parts[m]["Parte"] +  "," +
        gbl_Parts[m]["Codigo"] + "," +
        gbl_Parts[m]["Marca"] + "," +
        gbl_Parts[m]["Precio"] + "," +
        gbl_Parts[m]["Nota"] +
        "\n";
		
    var texto = "\n";
    for(var m = 0; m < gbl_Services.length; m++)
        texto += 
    	gbl_Services[m]["idServicio"] + "," +
        gbl_Services[m]["nombreServicio"] + "," +
        gbl_Services[m]["OdometroAhora"] +  "," +
        gbl_Services[m]["OdometroSiguiente"] +  "," +
        gbl_Services[m]["TipoOdometro"] +  "," +
        gbl_Services[m]["OdometroMi"] +  "," +
        gbl_Services[m]["OdometroKm"] +  "," +
        gbl_Services[m]["FechaAhora"] +  "," +
        gbl_Services[m]["FechaSiguiente"] +  "," +
        gbl_Services[m]["Costo"] +  "," +
        gbl_Services[m]["Nota"] +  "," +
        gbl_Services[m]["Alerta"] + 
        "\n";
 */