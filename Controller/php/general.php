<?php
    class generalFunctions{
        public function getListFull($sqlOps,$sql,$idDdl,$title,$idColumn,$idName){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="selectpicker" id="'.$idDdl.'" title="'.$title.'" data-style="btn-info" data-width="100%">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $list .= '<option value="'.$row[$idColumn].'">'.$row[$idName].'</option>';
                }
            }
            return $list .= '</select>';
        }
        public function getListFullWithSearch($sqlOps,$sql,$idDdl,$title,$idColumn,$idName){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="selectpicker" id="'.$idDdl.'" title="'.$title.'" data-style="btn-info" data-width="100%" data-live-search="true">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $list .= '<option value="'.$row[$idColumn].'">'.$row[$idName].'</option>';
                }
            }
            return $list .= '</select>';
        }
        public function getListWithExceptions($sqlOps,$sql,$idDdl,$title,$idColumn,$idName,$List){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="selectpicker" id="'.$idDdl.'" title="'.$title.'" data-style="btn-warning" data-width="100%">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if(in_array($row[$idName],$List)){
                        $list .= '<option value="'.$row[$idColumn].'">'.$row[$idName].'</option>';
                    }
                }
            }
            return $list .= '</select>';
        }
        public function getPluralNames($time, $unit){
            $result = $time . " - ";
            return $time > 1 ? $result . generalVariables::$unitServiceType[$unit] : $result . $unit;
        }
        public function nextServiceDate($current, $value, $unit){
            $effectiveDate = '';
            switch($unit){
                case 'Dia':
                    $effectiveDate = date('d-m-Y', strtotime($current. ' + '.$value.' days'));
                break;
                case 'Semana':
                    $effectiveDate = date('d-m-Y', strtotime($current. ' + '.($value*7).' days'));
                break;
                case 'Mes':
                    $effectiveDate = date('d-m-Y', strtotime("+".$value." months", strtotime($current)));
                break;
                case 'Año':
                    $effectiveDate = date('d-m-Y', strtotime("+".$value." years", strtotime($current)));
                break;
            }
            return $effectiveDate;
        }   
        public function nextOdometerMi($value, $unit){
            $nextOdometer = '';
            switch($unit){
                case 'Km':
                    $nextOdometer = round($value*0.621371);
                break;
                case 'Mi':
                    $nextOdometer = round($value);
                break;
            }
            return $nextOdometer;
        }  
        public function nextOdometerKm($value, $unit){
            $nextOdometer = '';
            switch($unit){
                case 'Km':
                    $nextOdometer = round($value);
                break;
                case 'Mi':
                    $nextOdometer = round($value*1.60934);
                break;
            }
            return $nextOdometer;
        }  
        public function get_array_calendar_event($sql,$sqlOps){
            $result = $sqlOps->sql_multiple_rows($sql);
            $path = array();
            $prevDate = '';
            $color = '';
            while($row = $result->fetch_assoc()){
                if($prevDate == $row["Fecha"]){
                    $color = "calendar_both";    
                }else{
                    $color = ($row["Tipo"] == 'Cumpleaños') ? "calendar_birthday" : "calendar_service";    
                }
                $prevDate = $row["Fecha"];
                $path[] = array(
                    "date"=>$row["Fecha"]
                    ,"title"=>$row["Tipo"]
                    ,"classname"=>$color
                    );
            }
            return $path;    
        }
    }
    class generalVariables{
        public static $unitServiceType = array(
            "Dia" => "Dias",
            "Semana" => "Semanas",
            "Mes" => "Meses",
            "Año" => "Años");
    } 