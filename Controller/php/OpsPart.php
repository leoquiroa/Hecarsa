<?php
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    include "../../Controller/php/general.php";
    $fns = new generalFunctions();    
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data)
    {
        //initial show
        case 'category_ddl':
            $output = $fns->getListFull(
                $sqlOps, 
                "CALL catalog_list('PartCategory')", 
                "ddl_part_category", 
                $_POST['title'], 
                "idCatalog", 
                "nameCatalog");
        break;
        case 'list_part':
            $result = $sqlOps->sql_multiple_rows("CALL part_list()");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td style="vertical-align: middle;">'.$row["categoryName"].'</td>
                            <td class="PartName" style="vertical-align: middle; cursor:pointer;" data-id0="'.$row["idPart"].'">'.$row["namePart"].'</td>
                            <td><img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width: 25px; height: 25px;"></td>
                            <td style="vertical-align: middle;">
                                <i class="fa fa-trash DeletePart" aria-hidden="true" data-id1="'.$row["idPart"].'"></i>
                            </td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTables-example" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Categoria</th>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Imagen</th>        
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }
        break;
        //save
        case 'add_part':
            $sql = "CALL part_add('".$_POST['name']."','".$_POST['categoryId']."','no_image.png',@si)";
            $output = $sqlOps->sql_exec_op_return($sql);            
        break;
        case 'part_category_add':
            $sql = "CALL catalog_add('".$_POST['nameCategoryPart']."','PartCategory',@si)";
            $output = $sqlOps->sql_exec_op_return($sql);
        break;
        //get
        case 'show_part':
            $sql = "CALL part_get(".$_POST['idPart'].")";
            $row = $sqlOps->sql_single_row($sql);
            if($row != ''){
                $res = array();
                $res[0][0] = $row["idPart"];
                $res[0][1] = $row["namePart"];
                $res[0][2] = $row["categoryName"];
                $res[0][3] = $row["imageUrl"];
                echo json_encode($res);
            }
        break;
        case 'table_part_category':
            $sql = "CALL catalog_list('PartCategory')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '';
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td contenteditable="true" class="EditCategoryPart" style="width: 170%;" data-id1="'.$row["idCatalog"].'" id="'.$row["idCatalog"].'cdp">'.$row["nameCatalog"].'</td>
                            <td style="vertical-align: middle; width: 30%; cursor:pointer;" class="DeleteCategoryPart" data-id1="'.$row["idCatalog"].'">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </td>
                        </tr>';    
                }    
            }
            $output .= '
                <table class="table table-condensed table-hover table-bordered scroll" style="font-size:12px; text-align:center;" id="tablePartCategory">
                    <tbody>';
            $output .= $list;
            $output .= '
                    </tbody>
                    </table>';
        break;
        //update
        case 'update_part':
            $sql = "CALL part_update('".$_POST['id']."','".$_POST['column_name']."','".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'update_part_category':
            $sql = "CALL catalog_update('".$_POST['idPartCategory']."','".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        //delete
        case 'remove_part_db':
            $sql = "CALL part_delete(".$_POST['idPart'].")";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'remove_part_file':
            $sql = "CALL part_get(".$_POST['idPart'].")";
            $row = $sqlOps->sql_single_row($sql);
            $output = 0;
            if($row != ''){
                unlink('../../Multimedia/Parts/'.$row['imageUrl']);
                $output = 1;
            }
        break;
        case 'category_part_delete':
            $sql = "CALL catalog_delete('".$_POST['IdCategoryPart']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
    }
    echo $output == '' ? '' : $output;