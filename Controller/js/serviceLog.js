var pathAutoComplete = "../Controller/php/OpsAutoCompleteIndex.php";
var pathIndexOps = "../Controller/php/OpsServiceLog.php";
var cacheName = {};
var cachePlate = {};

function ajax_call(sent_url,sent_data,div,async_){
    $.ajax({
        cache: false,
        async: async_,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function autoCompleteByName(){
    $("#searchByName").autocomplete({
        source: function( request, response ) {
            var term = request.term;
            if (term in cacheName) {
                response(cacheName[term]);
                return;
            }
            $.getJSON( 
                pathAutoComplete, 
                {type:'ByName', term: request.term}, 
                function(data,status,xhr){
                    cacheName[term] = data;
                    response(data);
                }
            );
        },
        minLength: 2,
        select: function(event, ui) {
            $('#searchByPlate').val('');
            fillVehicleTable(ui.item.id);
        }
    });
}
function autoCompleteByPlate(){
    $("#searchByPlate").autocomplete({
        source: function( request, response ) {
            var term = request.term;
            if (term in cachePlate) {
                response(cachePlate[term]);
                return;
            }
            $.getJSON( 
                pathAutoComplete, 
                {type:'ByPlate', term: request.term}, 
                function(data,status,xhr){
                    cachePlate[term] = data;
                    response(data);
                }
            );
        },
        minLength: 2,
        select: function(event, ui) {
            $('#searchByName').val('');
            fillVehicleTable(ui.item.id);
        }
    });
}
function fillVehicleTable(id){
    cleanVehicles();
    cleanServices();
    cleanParts();
    ajax_call(pathIndexOps,
        {type:'PersonHead'
        ,idPerson:id
    },"personHeadDiv",true);
    ajax_call(pathIndexOps,
        {type:'VehiclesTable'
        ,idPerson:id
    },"vehicleTableDiv",true);
}
function cleanVehicles(){
    $('#personHeadDiv').html('');
    $('#vehicleTableDiv').html('');
}
function cleanServices(){
    $('#vehicleHeadDiv').html('');
    $('#serviceTableDiv').html('');
}
function cleanParts(){
    $('#serviceHeadDiv').html('');
    $('#partTableDiv').html('');
}

$(document).ready(function() {
    autoCompleteByName();
    autoCompleteByPlate();
});
//OLD
$(document).on('click', '.vehicleList', function(){
    cleanServices();
    cleanParts();
    var id = $(this).data("id0");
    ajax_call(pathIndexOps,
        {type:'VehiclesHead'
        ,idVehicle:id
    },"vehicleHeadDiv",true);
    ajax_call(pathIndexOps,
        {type:'ServicesTable'
        ,idVehicle:id
    },"serviceTableDiv",true);
});
$(document).on('click', '.serviceList', function(){
    cleanParts();
    var id = $(this).data("id0");
    $.ajax({
        cache: false,
        async: true,
        url:pathIndexOps,  
        method:"POST",
        data:{
            type: 'ServiceAlert',
            idService:id
        },  
        dataType:"text",  
        success:function(d){  
            if(d.trim() !== ''){
                $("#alertForm").show();
                $("#alertText").text(d);
            }
        }  
    });
    ajax_call(pathIndexOps,
        {type:'ServiceHead'
        ,idService:id
    },"serviceHeadDiv",true);
    ajax_call(pathIndexOps,
        {type:'ServiceDetails'
        ,idService:id
    },"partTableDiv",true);
});
$(document).on('click', '#alertForm', function(){
    $("#alertForm").hide();
});
$(document).on('click', '#print', function(){
    $.ajax({
        cache: false,
        async: true,
        url:pathIndexOps,  
        method:"POST",
        data:{type:'print'},  
        dataType:"text",  
        success:function(d){}  
    });
});